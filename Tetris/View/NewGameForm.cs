﻿using System;
using System.Windows.Forms;
using Tetris.Contracts;
using Tetris.Presenter;

namespace Tetris.View
{
    public partial class NewGameForm : Form, INevGameView
    {
        public INewGamePresenter Presenter { get; set; }

        public NewGameForm()
        {
            InitializeComponent();
            Presenter = new NewGamePresenter(this);
        }

        public void ShowView()
        {
            Show();
        }

        public void CloseView()
        {
            Close();
        }

        public void HideView()
        {
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Presenter.StartNewGame(trackBar1.Value);
        }
    }
}
