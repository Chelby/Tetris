﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using NAudio.Wave;
using Tetris.Contracts;
using Tetris.Entities;
using Tetris.Entities.Shapes;
using Tetris.Presenter;
using Tetris.Properties;

namespace Tetris.View
{
    public partial class MainWindow : Form, IMainView
    {
        public IMainPresenter Presenter { get; set; }
        private PictureBox[,] _pictureBoxs;
        private PictureBox _nextShapePictureBox;
        private Dictionary<Type, Image> _dictionaryShape;
        private Dictionary<int, Image> _dictionaryBox;
        private readonly IWavePlayer _waveOutDevice;

        public MainWindow(INewGamePresenter parentPresenter, int speed)
        {
            InitializeComponent();
            InitializeGameField();
            InitializeNextShape();
            InitializeShapeDictionary();
            InitializeBoxDictionary();
            Presenter = new MainPresenter(this, parentPresenter, 1050 - speed);
            _waveOutDevice = new WaveOut();
            PlayMusic();
        }

        private void InitializeBoxDictionary()
        {
            _dictionaryBox = new Dictionary<int, Image>
            {
                {0, Resources.White},
                {1, Resources.Aqua},
                {2, Resources.Blue},
                {3, Resources.Orange},
                {4, Resources.Yellow},
                {5, Resources.Green},
                {6, Resources.Purple},
                {7, Resources.Red}
            };
        }

        private void InitializeShapeDictionary()
        {
            _dictionaryShape = new Dictionary<Type, Image>
            {
                {typeof (ShapeI), Resources.ShapeI},
                {typeof (ShapeJ), Resources.ShapeJ},
                {typeof (ShapeL), Resources.ShapeL},
                {typeof (ShapeT), Resources.ShapeT},
                {typeof (ShapeO), Resources.ShapeO},
                {typeof (ShapeS), Resources.ShapeS},
                {typeof (ShapeZ), Resources.ShapeZ}
            };
        }


        private void PlayMusic()
        {
            _waveOutDevice.Init(new AudioFileReader("music.mp3"));
            //_waveOutDevice.Play();
        }

        public void Draw(GameBoard gameBoard)
        {
            for (var i = 0; i < gameBoard.GetHeight(); i++)
            {
                for (var j = 0; j < gameBoard.GetWidth(); j++)
                {
                    _pictureBoxs[i, j].BackgroundImage = _dictionaryBox[gameBoard.Board[i, j]];
                }
            }
        }

        public void DrawNextShape(Type shapeType)
        {
            _nextShapePictureBox.Image = _dictionaryShape[shapeType];
        }

        public void ShowScore(int score)
        {
            label3.Text = score.ToString();
        }

        public void ShowResult(int score)
        {
            MessageBox.Show($"Ви набрали {score} очок");
        }

        private void InitializeGameField()
        {
            _pictureBoxs = new PictureBox[20, 10];
            for (var i = 0; i < 20; i++)
            {
                for (var j = 0; j < 10; j++)
                {
                    _pictureBoxs[i, j] = new PictureBox
                    {
                        Location = new System.Drawing.Point(10 + 20 * (j + 1), 10 + 20 * (i + 1)),
                        Size = new Size(20, 20),
                        TabIndex = 0,
                        TabStop = false
                    };
                    Controls.Add(_pictureBoxs[i, j]);
                }
            }
        }

        private void InitializeNextShape()
        {
            _nextShapePictureBox = new PictureBox
            {
                Location = new System.Drawing.Point(250, 50),
                Size = new Size(80, 80),
                TabIndex = 0,
                TabStop = false,
                BorderStyle = BorderStyle.Fixed3D
            };
            Controls.Add(_nextShapePictureBox);
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Z:
                    Presenter.RotateShapeToLeft();
                    break;
                case Keys.X:
                    Presenter.RotateShapeToRight();
                    break;
                case Keys.Left:
                    Presenter.MoveLeft();
                    break;
                case Keys.Right:
                    Presenter.MoveRight();
                    break;
                case Keys.Down:
                    Presenter.MoveDown();
                    break;
            }
        }

        public void ShowView()
        {
            Show();
        }

        public void CloseView()
        {
            _waveOutDevice.Stop();
            Dispose();
            Close();
        }

        public void HideView()
        {
            Hide();
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            Presenter.StopLoop();
            _waveOutDevice.Stop();
        }
    }
}
