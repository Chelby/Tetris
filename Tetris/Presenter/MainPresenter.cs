﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Tetris.Contracts;
using Tetris.Entities;

namespace Tetris.Presenter
{
    class MainPresenter : PresenterBase<IMainView, IMainPresenter>, IMainPresenter
    {
        private GameBoard _gameBoard;
        private Timer _timer;
        private bool _isShapeMove;
        private bool _isShapeRotate;
        private int _speed;
        private int _score;
        private INewGamePresenter _parentPresenter;
        
        public MainPresenter(IMainView view, INewGamePresenter parentPresenter, int speed) : base(view)
        {
            InitializeComponents(parentPresenter, speed);
            View.DrawNextShape(_gameBoard.NextShape.GetType());
        }

        private void InitializeComponents(INewGamePresenter parentPresenter, int speed)
        {
            _parentPresenter = parentPresenter;
            _gameBoard = new GameBoard();
            _isShapeMove = true;
            _isShapeRotate = true;
            _score = 0;
            _speed = speed;
            _timer = new Timer {Interval = speed};
            _timer.Tick += GameLoop;
            _timer.Start();
        }

        private void GameLoop(object sender, EventArgs args)
        {
            _isShapeMove = true;
            _isShapeRotate = true;
            _gameBoard.Combine();
            View.Draw(_gameBoard);
            var isFallen = _gameBoard.Gravity();
            if (isFallen)
            {
                DoWhenFall();
            }
        }

        private void DoWhenFall()
        {
            _gameBoard.ChangeShape();
            if (_gameBoard.CheckLoose())
            {
                StopLoop();
                return;
            }
            var countLines = _gameBoard.DestroyFillLines();
            _score += countLines;
            if (countLines > 0)
            {
                CalculateSpeed();
            }
            View.ShowScore(_score);
            View.DrawNextShape(_gameBoard.NextShape.GetType());
        }

        public void StopLoop()
        {
            _timer.Stop();
            View.ShowResult(_score);
            _parentPresenter.ShowNewGameSettings();
            View.CloseView();
        }

        public void RotateShapeToRight()
        {
            if (_isShapeRotate && _gameBoard.IsCanRotateToRight())
            {
                _gameBoard.CurrentShape.RotateToRight();
                _gameBoard.ClearRotatedRight();
                _isShapeRotate = false;
            }
        }

        public void RotateShapeToLeft()
        {
            if (_isShapeRotate && _gameBoard.IsCanRotateToLeft())
            {
                _gameBoard.CurrentShape.RotateToLeft();
                _gameBoard.ClearRotatedLeft();
                _isShapeRotate = false;
            }
        }

        public void MoveRight()
        {
            if (_isShapeMove && _gameBoard.IsCanMoveRight())
            {
                _gameBoard.CurrentShape.MoveRight();
                _gameBoard.ClearLeft();
                _isShapeMove = false;
            }
        }

        public void MoveLeft()
        {
            if (_isShapeMove && _gameBoard.IsCanMoveLeft())
            {
                _gameBoard.CurrentShape.MoveLeft();
                _gameBoard.ClearRight();
                _isShapeMove = false;
            }
        }

        public void MoveDown()
        {
            do
            {
                _gameBoard.Combine();
            } while (!_gameBoard.Gravity());
            DoWhenFall();
        }

        private void CalculateSpeed()
        {
            if (_speed > 50)
            {
                _speed -= 2;
                _timer.Interval = _speed;
            }
        }
    }
}
