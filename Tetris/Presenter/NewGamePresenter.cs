﻿using Tetris.Contracts;
using Tetris.View;

namespace Tetris.Presenter
{
    class NewGamePresenter : PresenterBase<INevGameView, INewGamePresenter>,INewGamePresenter
    {
        public NewGamePresenter(INevGameView view) : base(view)
        {
        }

        public void StartNewGame(int speed)
        {
            new MainWindow(this, speed).ShowView();
            View.HideView();
        }

        public void ShowNewGameSettings()
        {
            View.ShowView();
        }
    }
}
