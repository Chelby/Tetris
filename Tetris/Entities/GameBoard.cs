﻿using System;
using Tetris.Contracts;
using Tetris.Entities.Shapes;

namespace Tetris.Entities
{
    public class GameBoard
    {
        public int[,] Board { get; set; }
        public ShapeBase CurrentShape { get; set; }
        public ShapeBase NextShape { get; set; }
        private ShapeBase[] _shapeStorage;
        private readonly Random _random;

        public GameBoard()
        {
            InitialGameField();
            InitialShapeStorage();
            _random = new Random();
            CurrentShape = _shapeStorage[_random.Next(0, 4)];
            NextShape = _shapeStorage[_random.Next(0, 4)];
        }

        private void InitialGameField()
        {
            Board = new int[20, 10];
            for (var i = 0; i < Board.GetLength(0); i++)
            {
                for (var j = 0; j < Board.GetLength(1); j++)
                {
                    Board[i, j] = 0;
                }
            }
        }

        private void InitialShapeStorage()
        {
            _shapeStorage = new ShapeBase[7];
            _shapeStorage[0] = new ShapeI();
            _shapeStorage[1] = new ShapeJ();
            _shapeStorage[2] = new ShapeL();
            _shapeStorage[3] = new ShapeO();
            _shapeStorage[4] = new ShapeS();
            _shapeStorage[5] = new ShapeT();
            _shapeStorage[6] = new ShapeZ();
        }

        public void ChangeShape()
        {
            CurrentShape = NextShape;
            NextShape = (ShapeBase) Activator.CreateInstance(_shapeStorage[_random.Next(0, 7)].GetType());
            var randomNumber = _random.Next(0, 4);
            for (var i = 0; i < randomNumber; i++)
            {
                NextShape.RotateToRight();
            }
        }

        public int GetHeight()
        {
            return Board.GetLength(0);
        }

        public int GetWidth()
        {
            return Board.GetLength(1);
        }

        public void Combine()
        {
            var y = CurrentShape.Position.Y;
            var x = CurrentShape.Position.X;
            for (var i = 0; i < CurrentShape.LenghtY; i++)
            {
                for (var j = 0; j < CurrentShape.LenghtX; j++)
                {
                    if (y - i >= 0 && x + j < 10)
                    {
                        Board[y - i, x + j] |= CurrentShape.Shape[i, j];
                    }
                }
            }
        }

        private void ClearPreviousPosition()
        {
            for (var i = 0; i < CurrentShape.LenghtY; i++)
            {
                for (var j = 0; j < CurrentShape.LenghtX; j++)
                {
                    if (CurrentShape.Position.Y - (CurrentShape.LenghtY - i - 1) >= 0 && 
                        CurrentShape.Shape[CurrentShape.LenghtY - i - 1, j] == CurrentShape.Id)
                    {
                        Board[CurrentShape.Position.Y - (CurrentShape.LenghtY - i - 1) , CurrentShape.Position.X + j] = 0;
                    }
                }
            }
        }

        private bool IsCanBeMerged()
        {
            if (CurrentShape.Position.Y + 1 == 20)
            {
                return false;
            }
            for (var i = 0; i < CurrentShape.LenghtY; i++)
            {
                for (var j = 0; j < CurrentShape.LenghtX; j++)
                {
                    if (CurrentShape.Position.Y - i + 1 < 0 || CurrentShape.Position.X + j >= 10 ||
                        (i > 0 && (CurrentShape.Shape[i, j] == CurrentShape.Shape[i - 1, j])))
                    {
                        continue;
                    }
                    if ((CurrentShape.Shape[i, j] - Board[CurrentShape.Position.Y - i + 1,
                        CurrentShape.Position.X + j]) != CurrentShape.Id && CurrentShape.Shape[i, j] != 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool IsCanMoveLeft()
        {
            if (CurrentShape.Position.X - 1 < 0)
            {
                return false;
            }
            for (var i = 0; i < CurrentShape.LenghtY; i++)
            {
                for (var j = 0; j < CurrentShape.LenghtX; j++)
                {
                    if (CurrentShape.Position.Y - i < 0 || CurrentShape.Position.X - 1 < 0)
                    {
                        continue;
                    }
                    if (Board[CurrentShape.Position.Y - i, CurrentShape.Position.X + j - 1] != 0 &&
                        CurrentShape.Shape[i, j] != 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool IsCanMoveRight()
        {
            if (CurrentShape.Position.X + CurrentShape.LenghtX >= 10)
            {
                return false;
            }
            for (var i = 0; i < CurrentShape.LenghtY; i++)
            {
                for (var j = 0; j < CurrentShape.LenghtX; j++)
                {
                    if (CurrentShape.Position.Y - i < 0 || CurrentShape.Position.X + CurrentShape.LenghtX >= 10)
                    {
                        return false;
                    }
                    if (Board[CurrentShape.Position.Y - i, CurrentShape.Position.X + j + 1] != 0 &&
                        CurrentShape.Shape[i, j] != 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool IsCanRotateToRight()
        {
            var tempShape = new int[CurrentShape.LenghtX, CurrentShape.LenghtY];
            for (var i = 0; i < CurrentShape.Shape.GetLength(0); i++)
            {
                for (var j = 0; j < CurrentShape.Shape.GetLength(1); j++)
                {
                    tempShape[j, CurrentShape.Shape.GetLength(0) - i - 1] = CurrentShape.Shape[i, j];
                }
            }
            var posX = CurrentShape.Position.X + (CurrentShape.LenghtX/2 - 1);
            var posY = CurrentShape.Position.Y;
            for (var i = 0; i < tempShape.GetLength(0); i++)
            {
                for (var j = 0; j < tempShape.GetLength(1); j++)
                {
                    if (posY - i < 0 || posX + j >= 10 ||
                        Board[posY - i, posX + j] != 0 && 
                        tempShape[i, j] != 0)
                    {
                        return false;
                    }
                }
            }        
            return true;
        }

        public bool IsCanRotateToLeft()
        {
            var tempShape = new int[CurrentShape.LenghtX, CurrentShape.LenghtY];
            for (var i = 0; i < CurrentShape.Shape.GetLength(0); i++)
            {
                for (var j = 0; j < CurrentShape.Shape.GetLength(1); j++)
                {
                    tempShape[CurrentShape.Shape.GetLength(1) - j - 1, i] = CurrentShape.Shape[i, j];
                }
            }
            var posX = CurrentShape.Position.X - (CurrentShape.LenghtX / 2 - 1);
            var posY = CurrentShape.Position.Y;
            for (var i = 0; i < tempShape.GetLength(0); i++)
            {
                for (var j = 0; j < tempShape.GetLength(1); j++)
                {
                    if (posY - i < 0 || posX + j >= 10 || posX + j < 0 ||
                        Board[posY - i, posX + j] != 0 && 
                        tempShape[i, j] != 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public void ClearRotatedLeft()
        {
            var tempShape = new int[CurrentShape.LenghtX, CurrentShape.LenghtY];
            for (var i = 0; i < CurrentShape.Shape.GetLength(0); i++)
            {
                for (var j = 0; j < CurrentShape.Shape.GetLength(1); j++)
                {
                    tempShape[j, CurrentShape.Shape.GetLength(0) - i - 1] = CurrentShape.Shape[i, j];
                }
            }
            var posX = CurrentShape.Position.X - (CurrentShape.LenghtX/2 - 1);
            for (var i = 0; i < tempShape.GetLength(0); i++)
            {
                for (var j = 0; j < tempShape.GetLength(1); j++)
                {
                    if (CurrentShape.Position.Y - (CurrentShape.LenghtX - i - 1) >= 0 &&
                        tempShape[CurrentShape.LenghtX - i - 1, j] != 0 &&
                        posX + j < 10)
                    {
                        Board[CurrentShape.Position.Y - (CurrentShape.LenghtX - i - 1), posX + j] = 0;
                    }
                }
            }
        }

        public void ClearRotatedRight()
        {
            var tempShape = new int[CurrentShape.LenghtX, CurrentShape.LenghtY];
            for (var i = 0; i < CurrentShape.Shape.GetLength(0); i++)
            {
                for (var j = 0; j < CurrentShape.Shape.GetLength(1); j++)
                {
                    tempShape[CurrentShape.Shape.GetLength(1) - j - 1, i] = CurrentShape.Shape[i, j];
                }
            }
            var posX = CurrentShape.Position.X + (CurrentShape.LenghtX/2 - 1);
            for (var i = 0; i < tempShape.GetLength(0); i++)
            {
                for (var j = 0; j < tempShape.GetLength(1); j++)
                {
                    if (CurrentShape.Position.Y - (CurrentShape.LenghtX - i - 1) >= 0 &&
                        tempShape[CurrentShape.LenghtX - i - 1, j] != 0 &&
                        posX + j < 10)
                    {
                        Board[CurrentShape.Position.Y - (CurrentShape.LenghtX - i - 1), posX + j] = 0;
                    }
                }
            }
        }

        public void ClearLeft()
        {
            for (var i = 0; i < CurrentShape.LenghtY; i++)
            {
                for (var j = 0; j < CurrentShape.LenghtX; j++)
                {
                    if (CurrentShape.Shape[i,j] != 0 &&
                        CurrentShape.Position.Y - i >= 0)
                    {
                        Board[CurrentShape.Position.Y - i, CurrentShape.Position.X + j - 1] = 0;
                    }
                }
            }
        }

        public void ClearRight()
        {
            for (var i = 0; i < CurrentShape.LenghtY; i++)
            {
                for (var j = 0; j < CurrentShape.LenghtX; j++)
                {
                    if (CurrentShape.Shape[i, j] != 0 &&
                        CurrentShape.Position.Y - i >= 0)
                    {
                        Board[CurrentShape.Position.Y - i, CurrentShape.Position.X + j + 1] = 0;
                    }
                }
            }
        }

        public bool Gravity()
        {
            if (IsCanBeMerged())
            {
                ClearPreviousPosition();
                CurrentShape.Down();
                return false;
            }
            else
            {
                //ChangeShape();
                return true;
            }
        }

        public bool CheckLoose()
        {
            for (var i = 0; i < GetWidth(); i++)
            {
                if (Board[0, i] != 0)
                {
                    return true;
                }
            }
            return false;
        }

        public int DestroyFillLines()
        {
            var lineCount = 0;
            for (var i = 0; i < GetHeight(); i++)
            {
                bool isFullLine = true;
                for (var j = 0; j < GetWidth(); j++)
                {
                    if (Board[i, j] == 0)
                    {
                        isFullLine = false;
                        break;
                    }
                }
                if (isFullLine)
                {
                    ShiftLine(i);
                    lineCount++;
                }
            }
            return lineCount;
        }

        private void ShiftLine(int line)
        {
            for (var i = line; i > 0; i--)
            {

                for (var j = 0; j < GetWidth(); j++)
                {
                    Board[i, j] = Board[i - 1, j];
                }
            }
        }
    }
}
