﻿namespace Tetris.Entities
{
    public class Point
    {
        private int _x;
        private int _y;
        public int X
        {
            get { return _x; }
            set
            {
                if (value >= 0 && value < 10)
                    _x = value;
            }
        }

        public int Y
        {
            get { return _y; }
            set
            {
                if (value >= 0 && value < 20)
                    _y = value;
            }
        }
    }
}
