﻿using Tetris.Contracts;

namespace Tetris.Entities.Shapes
{
    class ShapeT : ShapeBase
    {
        public ShapeT() : base(2,3)
        {
            Id = 6;
            Shape[0, 0] = 0;
            Shape[0, 1] = 6;
            Shape[0, 2] = 0;
            Shape[1, 0] = 6;
            Shape[1, 1] = 6;
            Shape[1, 2] = 6;
        }
    }
}
