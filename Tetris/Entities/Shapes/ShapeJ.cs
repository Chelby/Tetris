﻿using Tetris.Contracts;

namespace Tetris.Entities.Shapes
{
    class ShapeJ : ShapeBase
    {
        public ShapeJ() : base(2,3)
        {
            Id = 2;
            Shape[0, 2] = 2;
            Shape[0, 1] = 0;
            Shape[0, 0] = 0;
            Shape[1, 0] = 2;
            Shape[1, 1] = 2;
            Shape[1, 2] = 2;
        }
    }
}
