﻿using Tetris.Contracts;

namespace Tetris.Entities.Shapes
{
    class ShapeO : ShapeBase
    {
        public ShapeO() : base(2,2)
        {
            Id = 4;
            Shape[0, 0] = 4;
            Shape[0, 1] = 4;
            Shape[1, 0] = 4;
            Shape[1, 1] = 4;
        }
    }
}
