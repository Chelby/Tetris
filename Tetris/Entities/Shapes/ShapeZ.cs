﻿using Tetris.Contracts;

namespace Tetris.Entities.Shapes
{
    class ShapeZ : ShapeBase
    {
        public ShapeZ() : base(2,3)
        {
            Id = 7;
            Shape[0, 0] = 0;
            Shape[0, 1] = 7;
            Shape[0, 2] = 7;
            Shape[1, 0] = 7;
            Shape[1, 1] = 7;
            Shape[1, 2] = 0;

           
        }
    }
}
