﻿using Tetris.Contracts;

namespace Tetris.Entities.Shapes
{
    class ShapeS : ShapeBase
    {
        public ShapeS() : base(2,3)
        {
            Id = 5;
            Shape[0, 0] = 5;
            Shape[0, 1] = 5;
            Shape[0, 2] = 0;
            Shape[1, 1] = 0;
            Shape[1, 1] = 5;
            Shape[1, 2] = 5;
        }
    }
}
