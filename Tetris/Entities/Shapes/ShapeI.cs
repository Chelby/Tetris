﻿using Tetris.Contracts;

namespace Tetris.Entities.Shapes
{
    class ShapeI : ShapeBase
    {
        public ShapeI() : base(1,4)
        {
            Id = 1;
            Shape[0, 0] = 1;
            Shape[0, 1] = 1;
            Shape[0, 2] = 1;
            Shape[0, 3] = 1;
        }
    }
}
