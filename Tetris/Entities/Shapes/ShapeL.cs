﻿using Tetris.Contracts;

namespace Tetris.Entities.Shapes
{
    class ShapeL : ShapeBase
    {
        public ShapeL() : base(2,3)
        {
            Id = 3;
            Shape[0, 0] = 3;
            Shape[0, 1] = 0;
            Shape[0, 2] = 0;
            Shape[1, 0] = 3;
            Shape[1, 1] = 3;
            Shape[1, 2] = 3;
        }
    }
}
