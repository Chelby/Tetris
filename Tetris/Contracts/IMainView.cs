﻿using System;
using Tetris.Entities;

namespace Tetris.Contracts
{
    interface IMainView : IView<IMainPresenter>
    {
        void Draw(GameBoard gameBoard);
        void DrawNextShape(Type shapeType);
        void ShowScore(int score);
        void ShowResult(int score);
    }
}
