﻿namespace Tetris.Contracts
{
    public interface IView<TPresenter>
    {
        TPresenter Presenter { get; set; }

        void ShowView();
        void CloseView();
        void HideView();
    }
}
