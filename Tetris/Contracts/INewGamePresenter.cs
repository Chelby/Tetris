﻿namespace Tetris.Contracts
{
    public interface INewGamePresenter
    {
        void StartNewGame(int speed);
        void ShowNewGameSettings();
    }
}
