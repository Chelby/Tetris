﻿namespace Tetris.Contracts
{
    public interface IMainPresenter
    {
        void RotateShapeToRight();
        void RotateShapeToLeft();
        void MoveRight();
        void MoveLeft();
        void MoveDown();
        void StopLoop();
    }
}
