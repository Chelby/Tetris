﻿using Tetris.Entities;

namespace Tetris.Contracts
{
    public abstract class ShapeBase
    {
        public int[,] Shape { get; set; }
        public Point Position { get; set; }
        public int LenghtX { get; set; }
        public int LenghtY { get; set; }
        public int Id { get; set; }
        
        protected ShapeBase(int i, int j)
        {
            Id = 1;
            Shape = new int[i, j];
            LenghtY = i;
            LenghtX = j;
            Position = new Point
            {
                X = 4,
                Y = 0
            };
        }

        public void RotateToRight()
        {
            LenghtY = Shape.GetLength(1);
            LenghtX = Shape.GetLength(0);
            var newShape = new int[LenghtY, LenghtX];
            for (var i = 0; i < Shape.GetLength(0); i++)
            {
                for (var j = 0; j < Shape.GetLength(1); j++)
                {
                    newShape[j,Shape.GetLength(0)-i-1] = Shape[i, j];
                }
            }
            Shape = newShape;
            Position.X = Position.X + (LenghtY/2 - 1);
        }

        public void RotateToLeft()
        {
            LenghtY = Shape.GetLength(1);
            LenghtX = Shape.GetLength(0);
            var newShape = new int[LenghtY, LenghtX];
            for (var i = 0; i < Shape.GetLength(0); i++)
            {
                for (var j = 0; j < Shape.GetLength(1); j++)
                {
                    newShape[Shape.GetLength(1) - j - 1, i] = Shape[i, j];
                }
            }
            Shape = newShape;
            Position.X = Position.X - (LenghtX / 2 - 1);
        }

        public void Down()
        {
            Position.Y++;
        }

        public void MoveLeft()
        {
            if (Position.X - 1 >= 0)
            {
                Position.X--;
            }
        }

        public void MoveRight()
        {
            if (Position.X + Shape.GetLength(1) < 10)
            {
                Position.X++;
            }
        }
    }
}
