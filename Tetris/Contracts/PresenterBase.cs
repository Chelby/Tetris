﻿using System;
namespace Tetris.Contracts
{
    public class PresenterBase<TView, TPresenter>
        where TView : IView<TPresenter>
        where TPresenter : class
    {
        public TView View { get; set; }

        public PresenterBase(TView view)
        {
            View = view;
            View.Presenter = this as TPresenter;
        } 
    }
}
